function updateForm(eventparam) {
  eventparam.preventDefault();
  eventparam.stopPropagation();

  var namecheck = document.forms["movieForm"]["name"].value;
    if (namecheck == "") {
      alert("Name must be filled out");
      return false;
    }

  var yearcheck = document.forms["movieForm"]["yearofRelease"].value;
    if (yearcheck == "") {
      alert("Year must be filled out");
      return false;
    }
    var ActorIds=$("#actors").val();
   var ProducerId=$("#producer").val();
   var MovieIds=$("#id").val();

    //var actsid=[];
   var YearOfRelease1 = document.forms["movieForm"]["yearofRelease"].value;
   var Yor = parseInt(YearOfRelease1);
  for (i = 0; i <ActorIds.length; i++) { 
    ActorIds[i] = parseInt(ActorIds[i]);  
   }   

   var Mid = parseInt(MovieIds);
   var Pid = parseInt(ProducerId);

   
   var Plot = $("#Plot").val() ;


   var movie = {
     id:Mid,
    Name: document.forms["movieForm"]["name"].value,
    YearofRelease: Yor,
    ActorIds:ActorIds,
    Plot:Plot,
    ProducerId:Pid
    }

    var movies =JSON.stringify(movie);


$.ajax
(
    {
      type: "PUT",
      url: "https://localhost:44382/movie/id",
      contentType: "application/json; charset=utf-8",
      data:movies,
      dataType: "json",
      success: function(response)
        {
          

        }

    }
)

  }//End of the submit form

$.ajax
(
    {
      type: "GET",
      url: "https://localhost:44382/producer/",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response)
{

var producers =JSON.parse(JSON.stringify(response));
var i;
var j;
var producerlist = ' ';
for (i = 0; i <producers.length; i++) {       

producerlist+=`<option value=${producers[i].id}>+${producers[i].name}</option>`;
//  actorlist+=`<option value=${actors[i].id}>${actors[i].name}</option>`;


}
$('#producer').append(producerlist);


},
     failure: function(response)
        {

        }
    }
);

$.ajax
(
    {
      type: "GET",
      url: "https://localhost:44382/actor/",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response)
{

var actors =JSON.parse(JSON.stringify(response));
var i;
var j;
var actorlist = ' ';
for (i = 0; i <actors.length; i++) {       

actorlist+=`<option value=${actors[i].id}>${actors[i].name}</option>`;
}
$('#actors').append(actorlist);


},
     failure: function(response)
        {

        }
    }
);
  


$.ajax
(
    {
      type: "GET",
      url: "https://localhost:44382/movie/",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function(response)
{

var movie =JSON.parse(JSON.stringify(response));
var i;
var j;
var movielist = ' ';
for (i = 0; i <movie.length; i++) {       

movielist+=`<option value=${movie[i].id}>+${movie[i].name}</option>`;
//  actorlist+=`<option value=${actors[i].id}>${actors[i].name}</option>`;


}
$('#id').append(movielist);


},
     failure: function(response)
        {

        }
    }
);

